package com.example.appprofileedits

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.appprofileedits.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_AppProfileEdits)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        init()
    }

    private fun init(){
        binding.editButton.setOnClickListener {
            val intent = Intent(this, EditProfileActivity::class.java)
            startActivityForResult(intent, 1)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1){
            if(resultCode == RESULT_OK){
                val firstName = data?.getStringExtra("FIRSTNAME")
                val lastName = data?.getStringExtra("LASTNAME")
                val age = data?.getStringExtra("AGE")
                val email = data?.getStringExtra("EMAIL")

                val date = data?.getStringExtra("DATE")
                
                val gender = data?.getStringExtra("GENDER")

                binding.firstNameTV.text = firstName
                binding.lastNameTV.text = lastName
                binding.ageTV.text = age
                binding.emailTV.text = email

                binding.dateTV.text = date
                
                binding.genderTv.text = gender
            }
            if (resultCode == RESULT_CANCELED){
                binding.firstNameTV.text = "No Info"
                binding.lastNameTV.text = "No Info"
                binding.ageTV.text = "No Info"
                binding.emailTV.text = "No Info"
            }
        }
    }
}