package com.example.appprofileedits

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.DatePicker
import android.widget.Toast
import com.example.appprofileedits.databinding.ActivityEditProfileBinding
import java.util.*

class EditProfileActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener {

    private lateinit var binding: ActivityEditProfileBinding

    var day = 0
    var month = 0
    var year = 0

    var savedDay = 0
    var savedMonth = 0
    var savedYear = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityEditProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init() {
        binding.saveButton.setOnClickListener {
            if (checkFields() && isEmailValid(binding.emailEditText.text.toString())) {
                sendInfo()
                finish()
            }else{
                Toast.makeText(this, "E-Mail Not Valid !", Toast.LENGTH_SHORT).show()
            }
        }

        binding.dateOfBirthButton.setOnClickListener {
            DatePickerDialog(this, this, year, month, day).show()
        }
    }


    private fun getDateTime() {
        val calendar = Calendar.getInstance()

        day = calendar.get(Calendar.DAY_OF_MONTH)
        month = calendar.get(Calendar.MONTH)
        year = calendar.get(Calendar.YEAR)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        savedDay = dayOfMonth
        savedMonth = month+1
        savedYear = year

        getDateTime()
    }

    private fun sendInfo() {
        val firstName = binding.firstNameEditText.text.toString().replaceFirstChar { it.uppercase() }
        val lastName = binding.lastNameEditText.text.toString().replaceFirstChar { it.uppercase() }
        val email = binding.emailEditText.text.toString()
        val age = binding.ageEditText.text.toString()

        val radioCheckedId = binding.radioGroup.checkedRadioButtonId
        val radioChecked = if (radioCheckedId == R.id.radioMale) binding.radioMale else binding.radioFemale

        val resultIntent = Intent(this, MainActivity::class.java)

        resultIntent.putExtra("FIRSTNAME", firstName)
        resultIntent.putExtra("LASTNAME", lastName)
        resultIntent.putExtra("EMAIL", email)
        resultIntent.putExtra("AGE", age)

        resultIntent.putExtra("DATE", "$savedDay \\ $savedMonth \\ $savedYear")

        resultIntent.putExtra("GENDER", radioChecked.text.toString())

        setResult(RESULT_OK, resultIntent)

    }

    private fun checkFields(): Boolean {
        val email = binding.emailEditText.text.toString()
        val firstName = binding.firstNameEditText.text.toString()
        val lastName = binding.lastNameEditText.text.toString()
        val age = binding.ageEditText.text.toString()
        var ifEmpty = true
        when {
            firstName.isEmpty() -> {
                Toast.makeText(this, "First Name is empty", Toast.LENGTH_SHORT).show()
                ifEmpty = false
            }
            lastName.isEmpty() -> {
                Toast.makeText(this, "Last Name is empty", Toast.LENGTH_SHORT).show()
                ifEmpty = false
            }
            age.isEmpty() -> {
                Toast.makeText(this, "Age is empty", Toast.LENGTH_SHORT).show()
                ifEmpty = false
            }
            email.isEmpty() -> {
                Toast.makeText(this, "Email is empty", Toast.LENGTH_SHORT).show()
                ifEmpty = false
            }
        }
        return ifEmpty
    }

    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}